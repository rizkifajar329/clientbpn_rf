﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientBPN_rf.Data_Model
{
    class SWARIS
    {
        private string nik_pemohon, nama_pemohon, alamat_pemohon, letak_tanah, surat_kuasa, sertifikat_asli,
            akta_jb, keterangan_warisan, keterangan_kematian, sspbphtp, pph;
        private int luas_tanah;
        private DateTime tanggal;

        public string NIKPemohon { get => nik_pemohon; set => nik_pemohon = value; }

        public string NamaPemohon { get => nama_pemohon; set => nama_pemohon = value; }

        public string AlamatPemohon { get => alamat_pemohon; set => alamat_pemohon = value; }

        public int LuasTanah { get => luas_tanah; set => luas_tanah = value; }

        public string LetakTanah { get => letak_tanah; set => letak_tanah = value; }

        public string SuratKuasa { get => surat_kuasa; set => surat_kuasa = value; }

        public string SertifikatAsli { get => sertifikat_asli; set => sertifikat_asli = value; }

        public string AktaJB { get => akta_jb; set => akta_jb = value; }

        public string KeteranganWrs { get => keterangan_warisan; set => keterangan_warisan = value; }

        public string KeteranganMati { get => keterangan_kematian; set => keterangan_kematian = value; }

        public string SSPBPHTP { get => sspbphtp; set => sspbphtp = value; }

        public string PPH { get => pph; set => pph = value; }

        public DateTime Tanggal { get => tanggal; set => tanggal = value; }
    }
}
