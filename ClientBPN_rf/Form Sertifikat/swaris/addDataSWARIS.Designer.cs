﻿namespace ClientBPN_rf.Form_Sertifikat.swaris
{
    partial class addDataSWARIS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label12 = new System.Windows.Forms.Label();
            this.pphbox = new System.Windows.Forms.ComboBox();
            this.sspbphtpbox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ketmatibox = new System.Windows.Forms.ComboBox();
            this.savebtn = new System.Windows.Forms.Button();
            this.datebox = new System.Windows.Forms.DateTimePicker();
            this.ketwarisbox = new System.Windows.Forms.ComboBox();
            this.aktajbbox = new System.Windows.Forms.ComboBox();
            this.sertifikataslibox = new System.Windows.Forms.ComboBox();
            this.skbox = new System.Windows.Forms.ComboBox();
            this.letakbox = new System.Windows.Forms.TextBox();
            this.luasbox = new System.Windows.Forms.TextBox();
            this.alamatbox = new System.Windows.Forms.TextBox();
            this.namabox = new System.Windows.Forms.TextBox();
            this.nikbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(663, 450);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(36, 17);
            this.label12.TabIndex = 117;
            this.label12.Text = "PPH";
            // 
            // pphbox
            // 
            this.pphbox.FormattingEnabled = true;
            this.pphbox.Location = new System.Drawing.Point(859, 450);
            this.pphbox.Name = "pphbox";
            this.pphbox.Size = new System.Drawing.Size(162, 24);
            this.pphbox.TabIndex = 116;
            // 
            // sspbphtpbox
            // 
            this.sspbphtpbox.FormattingEnabled = true;
            this.sspbphtpbox.Location = new System.Drawing.Point(859, 391);
            this.sspbphtpbox.Name = "sspbphtpbox";
            this.sspbphtpbox.Size = new System.Drawing.Size(162, 24);
            this.sspbphtpbox.TabIndex = 115;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(663, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 17);
            this.label13.TabIndex = 114;
            this.label13.Text = "SSPBPTHP";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(663, 337);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(145, 17);
            this.label11.TabIndex = 113;
            this.label11.Text = "Keterangan Kematian";
            // 
            // ketmatibox
            // 
            this.ketmatibox.FormattingEnabled = true;
            this.ketmatibox.Location = new System.Drawing.Point(859, 337);
            this.ketmatibox.Name = "ketmatibox";
            this.ketmatibox.Size = new System.Drawing.Size(162, 24);
            this.ketmatibox.TabIndex = 112;
            // 
            // savebtn
            // 
            this.savebtn.Location = new System.Drawing.Point(1036, 583);
            this.savebtn.Name = "savebtn";
            this.savebtn.Size = new System.Drawing.Size(102, 37);
            this.savebtn.TabIndex = 111;
            this.savebtn.Text = "Simpan Data";
            this.savebtn.UseVisualStyleBackColor = true;
            this.savebtn.Click += new System.EventHandler(this.savebtn_Click_1);
            // 
            // datebox
            // 
            this.datebox.Location = new System.Drawing.Point(859, 531);
            this.datebox.Name = "datebox";
            this.datebox.Size = new System.Drawing.Size(338, 22);
            this.datebox.TabIndex = 110;
            // 
            // ketwarisbox
            // 
            this.ketwarisbox.FormattingEnabled = true;
            this.ketwarisbox.Location = new System.Drawing.Point(859, 278);
            this.ketwarisbox.Name = "ketwarisbox";
            this.ketwarisbox.Size = new System.Drawing.Size(162, 24);
            this.ketwarisbox.TabIndex = 109;
            // 
            // aktajbbox
            // 
            this.aktajbbox.FormattingEnabled = true;
            this.aktajbbox.Location = new System.Drawing.Point(859, 227);
            this.aktajbbox.Name = "aktajbbox";
            this.aktajbbox.Size = new System.Drawing.Size(162, 24);
            this.aktajbbox.TabIndex = 108;
            // 
            // sertifikataslibox
            // 
            this.sertifikataslibox.FormattingEnabled = true;
            this.sertifikataslibox.Location = new System.Drawing.Point(859, 177);
            this.sertifikataslibox.Name = "sertifikataslibox";
            this.sertifikataslibox.Size = new System.Drawing.Size(162, 24);
            this.sertifikataslibox.TabIndex = 107;
            // 
            // skbox
            // 
            this.skbox.FormattingEnabled = true;
            this.skbox.Location = new System.Drawing.Point(859, 129);
            this.skbox.Name = "skbox";
            this.skbox.Size = new System.Drawing.Size(162, 24);
            this.skbox.TabIndex = 106;
            // 
            // letakbox
            // 
            this.letakbox.Location = new System.Drawing.Point(180, 334);
            this.letakbox.Name = "letakbox";
            this.letakbox.Size = new System.Drawing.Size(340, 22);
            this.letakbox.TabIndex = 105;
            // 
            // luasbox
            // 
            this.luasbox.Location = new System.Drawing.Point(180, 278);
            this.luasbox.Name = "luasbox";
            this.luasbox.Size = new System.Drawing.Size(340, 22);
            this.luasbox.TabIndex = 104;
            // 
            // alamatbox
            // 
            this.alamatbox.Location = new System.Drawing.Point(180, 227);
            this.alamatbox.Name = "alamatbox";
            this.alamatbox.Size = new System.Drawing.Size(340, 22);
            this.alamatbox.TabIndex = 103;
            // 
            // namabox
            // 
            this.namabox.Location = new System.Drawing.Point(180, 177);
            this.namabox.Name = "namabox";
            this.namabox.Size = new System.Drawing.Size(340, 22);
            this.namabox.TabIndex = 102;
            // 
            // nikbox
            // 
            this.nikbox.Location = new System.Drawing.Point(180, 129);
            this.nikbox.Name = "nikbox";
            this.nikbox.Size = new System.Drawing.Size(340, 22);
            this.nikbox.TabIndex = 101;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(663, 534);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 17);
            this.label10.TabIndex = 100;
            this.label10.Text = "Tanggal";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(663, 281);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 17);
            this.label9.TabIndex = 99;
            this.label9.Text = "Keterangan Warisan";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(663, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 17);
            this.label8.TabIndex = 98;
            this.label8.Text = "Akta Jual-Beli";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(663, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 17);
            this.label7.TabIndex = 97;
            this.label7.Text = "Sertifikat Asli";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(663, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 17);
            this.label6.TabIndex = 96;
            this.label6.Text = "Surat Kuasa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 337);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 17);
            this.label5.TabIndex = 95;
            this.label5.Text = "Letak Tanah";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 17);
            this.label4.TabIndex = 94;
            this.label4.Text = "Luas Tanah";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 230);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 93;
            this.label3.Text = "Alamat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 92;
            this.label2.Text = "NIK";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 91;
            this.label1.Text = "Nama";
            // 
            // addDataSWARIS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(229)))), ((int)(((byte)(239)))));
            this.ClientSize = new System.Drawing.Size(1291, 630);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pphbox);
            this.Controls.Add(this.sspbphtpbox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ketmatibox);
            this.Controls.Add(this.savebtn);
            this.Controls.Add(this.datebox);
            this.Controls.Add(this.ketwarisbox);
            this.Controls.Add(this.aktajbbox);
            this.Controls.Add(this.sertifikataslibox);
            this.Controls.Add(this.skbox);
            this.Controls.Add(this.letakbox);
            this.Controls.Add(this.luasbox);
            this.Controls.Add(this.alamatbox);
            this.Controls.Add(this.namabox);
            this.Controls.Add(this.nikbox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "addDataSWARIS";
            this.Text = "Add Data Sertifikat WARIS";
            this.Load += new System.EventHandler(this.addDataSWARIS_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox pphbox;
        private System.Windows.Forms.ComboBox sspbphtpbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ketmatibox;
        private System.Windows.Forms.Button savebtn;
        private System.Windows.Forms.DateTimePicker datebox;
        private System.Windows.Forms.ComboBox ketwarisbox;
        private System.Windows.Forms.ComboBox aktajbbox;
        private System.Windows.Forms.ComboBox sertifikataslibox;
        private System.Windows.Forms.ComboBox skbox;
        private System.Windows.Forms.TextBox letakbox;
        private System.Windows.Forms.TextBox luasbox;
        private System.Windows.Forms.TextBox alamatbox;
        private System.Windows.Forms.TextBox namabox;
        private System.Windows.Forms.TextBox nikbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}