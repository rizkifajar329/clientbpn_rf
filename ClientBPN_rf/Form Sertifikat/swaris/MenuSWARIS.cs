﻿using ClientBPN_rf.Data_Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientBPN_rf.Form_Sertifikat.swaris
{
    public partial class MenuSWARIS : Form
    {

        string url = "http://localhost:57622/Service1.svc/";
        Thread th;


        public MenuSWARIS()
        {
            InitializeComponent();
        }
        //method menampilkan data
        void loadData()
        {
            try
            {
                var webClient = new WebClient();

                var json = webClient.DownloadString(url + "getAllSertifikatWrs");
                var data = JsonConvert.DeserializeObject<List<SWARIS>>(json);
                dataGridView1.DataSource = data;

                DataGridViewButtonColumn button = new DataGridViewButtonColumn();
                {
                    button.Name = "button";
                    button.HeaderText = "Action";
                    button.Text = "Detail Data";

                    button.UseColumnTextForButtonValue = true; //dont forget this line
                    this.dataGridView1.Columns.Add(button);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //parsing data
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["button"].Index && e.RowIndex >= 0)
            {
                string nik = dataGridView1[0, e.RowIndex].Value.ToString();
                string nama = dataGridView1[1, e.RowIndex].Value.ToString();
                string alamat = dataGridView1[2, e.RowIndex].Value.ToString();
                string luas = dataGridView1[3, e.RowIndex].Value.ToString();
                string letak = dataGridView1[4, e.RowIndex].Value.ToString();
                string sk = dataGridView1[5, e.RowIndex].Value.ToString();
                string sa = dataGridView1[6, e.RowIndex].Value.ToString();
                string aktajb = dataGridView1[7, e.RowIndex].Value.ToString();
                string ketwrs = dataGridView1[8, e.RowIndex].Value.ToString();
                string ketmati = dataGridView1[9, e.RowIndex].Value.ToString();
                string sspbphtp = dataGridView1[10, e.RowIndex].Value.ToString();
                string pph = dataGridView1[11, e.RowIndex].Value.ToString();
                string tanggal = dataGridView1[12, e.RowIndex].Value.ToString();

                //DetailSWARIS detail = new DetailSWARIS(nik, nama, alamat, luas, letak, sk, sa, aktajb, ketwrs, ketmati, sspbphtp, pph, tanggal);
                //detail.Show();

            }
        }




        void menufresh()
        {
            Application.Run(new MenuSWARIS());
        }

        void refresh()
        {
            th = new Thread(menufresh);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            this.Close();
        }
        private void refreshBTN_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void MenuSWARIS_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void addbtn_Click(object sender, EventArgs e)
        {
            addDataSWARIS add = new addDataSWARIS();
            add.Show();
            this.Hide();
        }
    }
}
