﻿using ClientBPN_rf.Data_Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientBPN_rf.Form_Sertifikat.swaris
{
    public partial class addDataSWARIS : Form
    {

        string url = "http://localhost:57622/Service1.svc/";
        Thread th;
        public addDataSWARIS()
        {
            InitializeComponent();
        }
        void saveData()
        {
            var json = new WebClient();

            SWARIS swaris = new SWARIS();
            swaris.NamaPemohon = namabox.Text.ToString();
            swaris.NIKPemohon = nikbox.Text.ToString();
            swaris.AlamatPemohon = alamatbox.Text.ToString();
            swaris.LuasTanah = Convert.ToInt32(luasbox.Text.ToString());
            swaris.LetakTanah = letakbox.Text.ToString();
            swaris.SuratKuasa = skbox.Text.ToString();
            swaris.SertifikatAsli = sertifikataslibox.Text.ToString();
            swaris.AktaJB = aktajbbox.Text.ToString();
            swaris.KeteranganWrs = ketwarisbox.Text.ToString();
            swaris.KeteranganMati = ketmatibox.Text.ToString();
            swaris.SSPBPHTP = sspbphtpbox.Text.ToString();
            swaris.PPH = pphbox.Text.ToString();

            DateTime b = Convert.ToDateTime(datebox.Text.ToString());
            swaris.Tanggal = b;
            //untuk ubah tanggal dalam bentuk string dalam c#
            JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var data = JsonConvert.SerializeObject(swaris, microsoftDateFormatSettings);
            //ini untuk detil log date
            // {"Details":"Application started.","LogDate":"\/Date(1234656000000)\/"}

            json.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            json.UploadString(url + "addSertifikatWrs", "POST", data);
        }

        void loadData()
        {
            var webClient = new WebClient();

            var json1 = webClient.DownloadString(url + "getAllSuratKuasa");
            var data1 = JsonConvert.DeserializeObject<List<SuratKuasa>>(json1);

            foreach (SuratKuasa sk in data1)
            {
                skbox.Items.Add(sk.Status);
                skbox.SelectedIndex = 0;
            }

            var json2 = webClient.DownloadString(url + "getAllSertifikatAsli");
            var data2 = JsonConvert.DeserializeObject<List<KetSertifikatAsli>>(json2);

            foreach (KetSertifikatAsli sa in data2)
            {
                sertifikataslibox.Items.Add(sa.Status);
                sertifikataslibox.SelectedIndex = 0;
            }

            var json3 = webClient.DownloadString(url + "getAllAktaJB");
            var data3 = JsonConvert.DeserializeObject<List<AktaJB>>(json3);

            foreach (AktaJB aktajb in data3)
            {
                aktajbbox.Items.Add(aktajb.Status);
                aktajbbox.SelectedIndex = 0;
            }

            var json4 = webClient.DownloadString(url + "getAllKetWrs");
            var data4 = JsonConvert.DeserializeObject<List<KetWaris>>(json4);

            foreach (KetWaris kw in data4)
            {
                ketwarisbox.Items.Add(kw.Status);
                ketwarisbox.SelectedIndex = 0;
            }

            var json5 = webClient.DownloadString(url + "getAllKetMati");
            var data5 = JsonConvert.DeserializeObject<List<KetMati>>(json5);

            foreach (KetMati km in data5)
            {
                ketmatibox.Items.Add(km.Status);
                ketmatibox.SelectedIndex = 0;
            }

            var json6 = webClient.DownloadString(url + "getAllSSBPHTP");
            var data6 = JsonConvert.DeserializeObject<List<SSPBPHTP>>(json6);

            foreach (SSPBPHTP ssp in data6)
            {
                sspbphtpbox.Items.Add(ssp.Status);
                sspbphtpbox.SelectedIndex = 0;
            }

            var json7 = webClient.DownloadString(url + "getAllPPH");
            var data7 = JsonConvert.DeserializeObject<List<PPH>>(json7);

            foreach (PPH pph in data7)
            {
                pphbox.Items.Add(pph.Status);
                pphbox.SelectedIndex = 0;
            }
        }

        private void addDataSWARIS_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void savebtn_Click(object sender, EventArgs e)
        {
            MenuSWARIS menu = new MenuSWARIS();
            menu.Show();
            this.Hide();
            saveData();
        }

        private void addDataSWARIS_Load_1(object sender, EventArgs e)
        {
            loadData();
        }

        private void savebtn_Click_1(object sender, EventArgs e)
        {
            MenuSWARIS menu = new MenuSWARIS();
            menu.Show();
            this.Hide();
            saveData();
        }
    }
}
